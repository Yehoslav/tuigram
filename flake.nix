{
  description = "A fork of arigram";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # see https://github.com/nix-community/poetry2nix/tree/master#api for more functions and examples.
        pkgs = nixpkgs.legacyPackages.${system};
        inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; }) mkPoetryApplication;
      in
        {
        packages = {
          tuigram = mkPoetryApplication { projectDir = self; };
          default = self.packages.${system}.tuigram;
        };

        devShells.default = 
          with pkgs; let
            py-packs = pypac:
              with pypac; [
                pylsp-mypy
                python-lsp-server
                python-lsp-ruff
                # ipython
              ];
            my-python = python311.withPackages py-packs;
          in
            pkgs.mkShell {
              inputsFrom = [ self.packages.${system}.tuigram ];
              packages = 
                with pkgs; [
                  poetry
                  pyright
                  ruff
                  just
                  black
                  pylint
                  jq
                ];
              buildInputs = [
                my-python
              ];
            };
      });
}
