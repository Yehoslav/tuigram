from pathlib import Path
import logging

from collections.abc import Callable
from typing import Literal

from tuigram.keys.types import Key, KeyType, ModKey
from tuigram.unicode import graphemes as gph

type Editor = Literal["ncurses", "external"]
type TextObject = Literal["char", "word", "text"]
type CursorDirection = Literal["forward", "backward"]

log = logging.getLogger(__name__)

class InputAction:
    pass


class InputBox:

    def __init__(self, history_file, initial_mode):
        self.buff = InputBuffer()
        self.history = InputHistory(history_file)
        self.count = None
        self.mode = initial_mode

        self._viewbox_anchor_right = None

    def view(self, max_width: int) -> tuple[str, int]:
        view = ""
        term_cursor_pos = 0

        graphemes_len = gph.gr_term_len(self.buff.graphemes[:])
        before_cursor = self.buff.graphemes[: self.buff.cursor]

        if graphemes_len < max_width - 1:
            view = "".join((g.terminal_view() for g in self.buff.graphemes))
            term_cursor_pos = gph.gr_term_len(before_cursor)
            return view, term_cursor_pos

        if self._viewbox_anchor_right is None:

            if gph.gr_term_len(before_cursor) < max_width - 1:
                graphemes_len = 0
                for i, gr in enumerate(self.buff.graphemes):
                    if graphemes_len + gr.terminal_width >= max_width:
                        break
                    self._viewbox_anchor_right = i
                    graphemes_len += gr.terminal_width
            else:
                self._viewbox_anchor_right = self.buff.cursor

        elif self.buff.cursor > self._viewbox_anchor_right:
            self._viewbox_anchor_right = self.buff.cursor

        elif self.buff.cursor < self._viewbox_anchor_right:
            curs_anchor_range = self.buff.graphemes[self.buff.cursor:self._viewbox_anchor_right+1]
            if gph.gr_term_len(curs_anchor_range) >= max_width:
                graphemes_len = 0
                self._viewbox_anchor_right = self.buff.cursor
                for i, gr in enumerate(curs_anchor_range):
                    if graphemes_len + gr.terminal_width >= max_width:
                        break
                    self._viewbox_anchor_right += 1
                    graphemes_len += gr.terminal_width


        before_anchor = self.buff.graphemes[:self._viewbox_anchor_right]
        term_cursor_pos = max_width - 1
        graphemes_len = 0

        for i, gr in reversed(list(enumerate(before_anchor))):
            if graphemes_len + gr.terminal_width >= max_width:
                gr_view = gph.decode_graphemes(view)
                gr_view_len = gph.gr_term_len(gr_view)
                if gr_view_len < (max_width - 1):
                    view = ('⍇' * (max_width - gr_view_len - 1)) + view
                break
            view = gr.terminal_view() + view
            graphemes_len += gr.terminal_width
            if i >= self.buff.cursor:
                term_cursor_pos = max_width - graphemes_len - 1

        return view, term_cursor_pos

    def clear(self):
        self.buff.text = ""
        self.history.reset()

    def execute_actions(self, action: InputAction) -> InputAction | None:
        if isinstance(action, Queue):
            for act in action.actions:
                self.execute_actions(act)
            return

        skip_reset = False
        match action:
            case InsertText(text=text):
                self.buff.insert_text(text)
                self.history.filter(self.buff.text)

            case Delete(direction="backward", obj="char"):
                self.buff.rm_prev_char(count=self.count or 1)
            case Delete(direction="forward", obj="char"):
                self.buff.rm_next_char(count=self.count or 1)

            case Delete(direction="backward", obj="word"):
                self.buff.rm_back_word(count=self.count or 1)
            case Delete(direction="forward", obj="word"):
                self.buff.rm_forward_word(count=self.count or 1)

            case Delete(direction="forward", obj="text"):
                self.buff.rm_forward_text()

            case MoveCursor(direction="backward", obj="char"):
                self.buff.prev_char(count=self.count or 1)
            case MoveCursor(direction="forward", obj="char"):
                self.buff.next_char(count=self.count or 1)

            case MoveCursor(direction="backward", obj="word"):
                self.buff.back_word(count=self.count or 1)
            case MoveCursor(direction="forward", obj="word"):
                self.buff.forward_word(count=self.count or 1)

            case MoveCursor(direction="backward", obj="text"):
                self.buff.go_to_text_start()
            case MoveCursor(direction="forward", obj="text"):
                self.buff.go_to_text_end()

            case Submit():
                return action

            case ClearText():
                self.clear()

            case Cancel():
                if self.count is None:
                    return action

            case Count(amount=amount):
                if self.count is None:
                    self.count = amount if amount > 0 else None
                else:
                    self.count = self.count * 10 + amount
                skip_reset = True

            case SwitchMode(mode=mode):
                self.mode = mode
                skip_reset = True

            case ExternalEditor():
                return action

            case Custom(edit=edit):
                tmp_buffer = edit(self.buff.copy(), self.count or 1)
                if tmp_buffer.cursor >= 0 and tmp_buffer.cursor <= len(
                    tmp_buffer.graphemes
                ):
                    self.buff = tmp_buffer
                # FIXME: Else branch warning the user that the given function is bad

            case NavigateHistory(next=True):
                self.buff.text = self.history.next_entry()
            case NavigateHistory():
                self.buff.text = self.history.prev_entry()

        if not skip_reset:
            self.count = None


class InputHistory:
    position: int
    path: Path
    entries: list[str]
    filtered: list[str]
    filter_text: str

    def __init__(self, history_file: Path):
        if history_file.exists():
            self.path = history_file
        else:
            raise ValueError(f"{history_file} does not exist")

        self.position = -1
        self.filter_text = ""
        self.read_history()

    def read_history(self):
        with open(self.path, "r", encoding="utf-8") as hf:
            self.entries = hf.readlines()[::-1]
            self.filtered = self.entries.copy()

    def next_entry(self) -> str:
        if self.position >= 0:
            self.position -= 1
        return self.entry

    def prev_entry(self) -> str:
        if self.position < len(self.filtered):
            self.position += 1
        return self.entry

    def reset(self):
        self.filtered = self.entries.copy()

    def filter(self, filter_text: str):
        self.filtered = [
            entry for entry in self.entries if filter_text in entry
        ]
        self.position = -1
        self.filter_text = filter_text

    @property
    def entry(self) -> str:
        if self.position == -1:
            return self.filter_text
        return self.filtered[self.position]


class InputBuffer:
    def __init__(self, text: str = "", cursor: int = 0):
        self._graphemes = gph.decode_graphemes(text)
        self._cursor = cursor

    def __repr__(self):
        return f"InputBuffer(text='{self.text}', cursor={self.cursor}, graphemes={self._graphemes})"

    def go_to_text_start(self) -> None:
        self._cursor = 0

    def go_to_text_end(self) -> None:
        self._cursor = len(self._graphemes)

    def insert_text(self, text: str) -> None:
        """Insert the given text at the current cursor position."""
        graphemes = gph.decode_graphemes(text)
        self._graphemes = (
            self._graphemes[: self._cursor]
            + graphemes
            + self._graphemes[self._cursor :]
        )
        self._cursor += len(graphemes)

    def prev_char(self, count: int = 1) -> bool:
        """Move cursor back `count` characters."""
        last = self._cursor
        self._cursor = max(0, self._cursor - count)
        return last != self._cursor

    def next_char(self, count: int = 1) -> bool:
        """Move cursor forward `count` characters."""
        last = self._cursor
        self._cursor = min(len(self._graphemes), self._cursor + count)
        return last != self._cursor

    def rm_prev_char(self, count: int = 1) -> None:
        """Remove backward `count` characters."""
        if self._cursor - count >= 0:
            self._graphemes = (
                self._graphemes[: self._cursor - count]
                + self._graphemes[self._cursor :]
            )
            self._cursor = max(0, self._cursor - count)
        else:
            self._graphemes = self._graphemes[self._cursor :]
            self._cursor = 0

    def peek_ch(self, forward: bool = True) -> str | None:
        if forward:
            return (
                self._cursor < len(self._graphemes)
                and self._graphemes[self._cursor].text
            )
        return self._cursor > 0 and self._graphemes[self._cursor - 1].text

    def skip_white_space(self, forward: bool = True) -> None:
        func = self.next_char if forward else self.prev_char
        while self.peek_ch(forward) == " ":
            if not func():
                return

    def rm_next_char(self, count: int = 1) -> None:
        """Remove forward `count` characters."""
        self._graphemes = (
            self._graphemes[: self._cursor] + self._graphemes[self._cursor + count :]
        )

    def back_word(self, count: int = 1) -> None:
        for _ in range(count):
            self.skip_white_space(False)
            for gr in self._graphemes[self._cursor - 1 :: -1]:
                if gr.text == " ":
                    break
                self.prev_char()

    def rm_forward_word(self, count: int = 1) -> None:
        if len(self._graphemes) == self._cursor:
            return

        cursor_save = self._cursor

        delete_begin = self._cursor
        self.forward_word(count)
        delete_end = self._cursor

        self._graphemes = self._graphemes[:delete_begin] + self._graphemes[delete_end:]
        self._cursor = cursor_save

    def rm_back_word(self, count: int = 1) -> None:
        cursor_save = self._cursor
        if not self.prev_char():
            return

        self.back_word(count)

        self._graphemes = (
            self._graphemes[: self._cursor] + self._graphemes[cursor_save:]
        )

    def rm_forward_text(self, count: int = 1) -> None:
        self._graphemes = self._graphemes[: self._cursor]

    def word_end(self, count: int = 1) -> None:
        for _ in range(count):
            self.skip_white_space()
            self.next_char()
            for gr in self._graphemes[self._cursor :]:
                if gr.text == " ":
                    break
                self.next_char()

    def forward_word(self, count: int = 1) -> None:
        log.info("forward start: %s", self.graphemes)
        for _ in range(count):
            for gr in self._graphemes[self._cursor :]:
                self.next_char()
                if gr.text == " ":
                    break
            self.skip_white_space()

    @property
    def text(self) -> str:
        return "".join(map(str, self._graphemes))

    @property
    def graphemes(self) -> list[gph.Grapheme]:
        return self._graphemes

    @text.setter
    def text(self, new_text):
        """Set the text, placing the cursor at the end."""
        self._graphemes = gph.decode_graphemes(new_text)
        self._cursor = len(self._graphemes)

    @property
    def cursor(self) -> int:
        return self._cursor

    def copy(self):
        return InputBuffer(self.text, self._cursor)


def word_end(ib: InputBuffer, count: int = 1) -> InputBuffer:
    ib.word_end(count)
    return ib


class InsertText(InputAction):
    def __init__(self, text):
        self.text = text


class NavigateHistory(InputAction):
    def __init__(self, next: bool):
        self.next = next


class Custom(InputAction):
    def __init__(self, edit: Callable[[InputBuffer, int], InputBuffer]):
        self.edit = edit


class MoveCursor(InputAction):
    def __init__(self, direction: CursorDirection, obj: TextObject):
        self.direction = direction
        self.obj = obj


class ClearText(InputAction):
    pass


class Delete(InputAction):
    def __init__(self, direction: CursorDirection, obj: TextObject):
        self.direction = direction
        self.obj = obj


class ExternalEditor(InputAction):
    def __init__(self, submit_after_edit: bool = False):
        self.submit_after_edit = submit_after_edit


class SwitchMode(InputAction):
    def __init__(self, mode: str) -> None:
        self.mode = mode


class Queue(InputAction):
    def __init__(self, actions: list[InputAction]):
        self.actions = actions

    def push(self, action: InputAction) -> None:
        self.actions.append(action)


class Count(InputAction):
    def __init__(self, amount) -> None:
        self.amount = amount


class Submit(InputAction):
    pass


class Cancel(InputAction):
    pass


class Skip(InputAction):
    pass


def vim_key_handler(key: Key, mode: str) -> InputAction:
    if mode == "insert":
        if key.type == KeyType.Text and key.pasted is True:
            return InsertText(key.value)

        if (
            len(key.mods) == 0 or key.mods == (ModKey.SHIFT,)
        ) and key.value.isprintable():
            return InsertText(key.value)

        if key.mods == (ModKey.CONTROL,):
            match key.type:
                case KeyType.LEFT_SQUARE_BRACKET:  # ESC
                    return SwitchMode("normal")
                case KeyType.J:  # <Return>
                    return Submit()

                case KeyType.A:
                    return MoveCursor(direction="backward", obj="text")
                case KeyType.E:
                    return MoveCursor(direction="forward", obj="text")

                case KeyType.N:
                    return NavigateHistory(next=True)
                case KeyType.P:
                    return NavigateHistory(next=False)

                case KeyType.H:  # ^H | ^BS they are the same
                    return Delete(direction="backward", obj="word")

                case KeyType.B | KeyType.LEFT:  # ^B | <A-left>
                    return MoveCursor(direction="backward", obj="word")
                case KeyType.W | KeyType.RIGHT:  # ^W | <A-right>
                    return MoveCursor(direction="forward", obj="word")

        match key.type:
            case KeyType.BS:
                return Delete(direction="backward", obj="char")

            case KeyType.LEFT:
                return MoveCursor(direction="backward", obj="char")
            case KeyType.RIGHT:
                return MoveCursor(direction="forward", obj="char")

            case KeyType.DELETE if key.mods == (ModKey.ALT,):  # <A-Delete>
                return Delete(direction="forward", obj="word")

            case KeyType.DELETE:
                return Delete(direction="forward", obj="char")

            case KeyType.E if key.mods == (ModKey.ALT,):
                return ExternalEditor()

            # HACK: Any other keys are displayed as text. It's to allow pasting.
            #       Idealy only printable characters should be saved to the buffer.
            case _:
                return Skip()

    if mode == "normal":
        if key.mods == (ModKey.CONTROL,):
            match key.type:
                case KeyType.LEFT_SQUARE_BRACKET:  # ESC
                    return Cancel()

                case KeyType.J:  # <Return>
                    return Submit()

                case KeyType.N:  # ^N
                    return NavigateHistory(next=True)
                case KeyType.P:  # ^P
                    return NavigateHistory(next=False)

        match key.type:
            case KeyType.I:  # i
                return SwitchMode("insert")
            case KeyType.A:  # a
                return Queue(
                    [
                        MoveCursor(direction="forward", obj="char"),
                        SwitchMode("insert"),
                    ]
                )

            case KeyType.E if key.mods == (ModKey.ALT,):  # <A-e>
                return ExternalEditor()

            case KeyType.I if key.mods == (ModKey.SHIFT,):  # I
                return Queue(
                    [
                        MoveCursor(direction="backward", obj="text"),
                        SwitchMode("insert"),
                    ]
                )
            case KeyType.A if key.mods == (ModKey.SHIFT,):  # A
                return Queue(
                    [
                        MoveCursor(direction="forward", obj="text"),
                        SwitchMode("insert"),
                    ]
                )

            case KeyType.C if key.mods == (ModKey.SHIFT,):  # C
                return Queue(
                    [
                        Delete(direction="forward", obj="text"),
                        SwitchMode("insert"),
                    ]
                )
            case KeyType.D if key.mods == (ModKey.SHIFT,):  # D
                return Delete(direction="forward", obj="text")

            case KeyType.C:  # c
                return SwitchMode("change")
            case KeyType.D:  # d
                return SwitchMode("delete")

            case KeyType.E:  # e
                return Custom(edit=word_end)

            case KeyType.H:  # h
                return MoveCursor(direction="backward", obj="char")
            case KeyType.L:  # l
                return MoveCursor(direction="forward", obj="char")

            case KeyType.B:  # b
                return MoveCursor(direction="backward", obj="word")
            case KeyType.W:  # w
                return MoveCursor(direction="forward", obj="word")

            case KeyType.DIGIT_0:  # 0
                return MoveCursor(direction="backward", obj="text")
            case KeyType.DOLLAR_SIGN:  # $
                return MoveCursor(direction="forward", obj="text")

            case k_type if "DIGIT_" in k_type.name:
                return Count(int(k_type.value))
        return Skip()

    if mode in ("delete", "change"):
        actions = Queue([])
        match key.type:
            case KeyType.D if mode == "delete":  # d
                actions.push(ClearText())
            case KeyType.C if mode == "change":  # c
                actions.push(ClearText())

            case KeyType.H:
                actions.push(Delete(direction="backward", obj="char"))
            case KeyType.L:
                actions.push(Delete(direction="forward", obj="char"))

            case KeyType.W:
                actions.push(Delete(direction="forward", obj="word"))
            case KeyType.B:
                actions.push(Delete(direction="backward", obj="word"))

            case k_type if "DIGIT_" in k_type.name:
                return Count(int(k_type.value))

        actions.push(SwitchMode("normal" if mode == "delete" else "insert"))

        return actions


def tuigram_key_handler(key: Key, mode: str) -> InputAction:
    if (
        len(key.mods) == 0 or key.mods == (ModKey.SHIFT,)
    ) and key.value.isprintable():
        return InsertText(key.value)

    if key.mods == (ModKey.CONTROL,):
        match key.type:
            case KeyType.LEFT_SQUARE_BRACKET:  # ESC
                return Cancel()
            case KeyType.J:  # <Return>
                return Submit()
            case KeyType.A:  # ^A
                return MoveCursor(direction="backward", obj="text")
            case KeyType.F:  # ^E
                return MoveCursor(direction="forward", obj="text")
            case KeyType.E:  # <A-e>
                return ExternalEditor()
            case KeyType.N:  # ^N
                return NavigateHistory(next=True)
            case KeyType.P:  # ^P
                return NavigateHistory(next=False)

    if key.mods == (ModKey.ALT,):
        match key.type:
            case KeyType.H | KeyType.RIGHT:
                return MoveCursor(direction="backward", obj="word")
            case KeyType.L | KeyType.LEFT:
                return MoveCursor(direction="forward", obj="word")
            case KeyType.BS:  # <BS>
                return Delete(direction="backward", obj="word")
            case KeyType.D | KeyType.DELETE:  # <A-Delete>
                return Delete(direction="forward", obj="word")

    match key:
        case KeyType.BS:
            return Delete(direction="backward", obj="char")
        case Key(type=KeyType.H, mods=(ModKey.CONTROL,)) | Key(
            type=KeyType.RIGHT, mods=tuple()
        ):
            return MoveCursor(direction="backward", obj="char")
        case Key(type=KeyType.L, mods=(ModKey.CONTROL,)) | Key(
            type=KeyType.LEFT, mods=tuple()
        ):
            return MoveCursor(direction="forward", obj="char")
        case Key(type=KeyType.D, mods=(ModKey.CONTROL,)) | Key(
            type=KeyType.DELETE, mods=tuple()
        ):
            return Delete(direction="forward", obj="char")

    return Skip()
