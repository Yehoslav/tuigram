from tuigram.keys.types import Key, KeyType, ModKey

# from types import Key, KeyType, ModKey


def read_key_sequence(win) -> list[bytes]:
    keys = []

    win.nodelay(False)
    while True:
        key = win.getch()
        if key == -1:
            break
        win.nodelay(True)
        keys.append(key)

    return [key.to_bytes() for key in keys]


def detect_bracketed_input(keys: list[bytes]) -> tuple[Key | None, bool]:
    """
    detectBracketedPaste detects an input pasted while bracketed
    paste mode was enabled.

    ported from bubbletea key_sequences
    """

    bp_start = b"\x1b[200~"
    if (
        len(keys) < len(bp_start)
        or b"".join(keys[: len(bp_start)]) != bp_start
    ):
        return None, True

    keys = keys[len(bp_start) :]

    bp_end = b"\x1b[201~"
    try:
        idx = b"".join(keys).index(bp_end)
    except ValueError:
        return None, False

    input_len = len(bp_start) + idx + len(bp_end)
    paste = keys[:idx]

    key = Key(
        type=KeyType.Text,
        value=b"".join(paste).decode("utf-8"),
        mods=tuple(),
        pasted=True,
    )

    # return True, input_len, key
    return key, True


def detect_printable_key_type(character: bytes) -> KeyType | None:

    if b"\x20" <= character <= b"\x7f":
        if b"A" <= character <= b"Z":
            return KeyType[character.decode("utf-8")], ModKey.SHIFT

        if b"a" <= character <= b"z":
            return KeyType[chr(ord(character) - 32)], None

    k_type = {
        b" ": KeyType.SPACE,
        b"!": KeyType.EXCLAMATION_MARK,
        b'"': KeyType.QUOTATION_MARK,
        b"#": KeyType.NUMBER_SIGN,
        b"$": KeyType.DOLLAR_SIGN,
        b"%": KeyType.PERCENT_SIGN,
        b"&": KeyType.AMPERSAND,
        b"'": KeyType.APOSTROPHE,
        b"(": KeyType.LEFT_PARENTHESIS,
        b")": KeyType.RIGHT_PARENTHESIS,
        b"*": KeyType.ASTERISK,
        b"+": KeyType.PLUS_SIGN,
        b",": KeyType.COMMA,
        b"-": KeyType.HYPHEN,
        b".": KeyType.PERIOD,
        b"/": KeyType.SLASH,
        b"0": KeyType.DIGIT_0,
        b"1": KeyType.DIGIT_1,
        b"2": KeyType.DIGIT_2,
        b"3": KeyType.DIGIT_3,
        b"4": KeyType.DIGIT_4,
        b"5": KeyType.DIGIT_5,
        b"6": KeyType.DIGIT_6,
        b"7": KeyType.DIGIT_7,
        b"8": KeyType.DIGIT_8,
        b"9": KeyType.DIGIT_9,
        b":": KeyType.COLON,
        b";": KeyType.SEMICOLON,
        b"<": KeyType.LESS_THAN,
        b"=": KeyType.EQUALS_SIGN,
        b">": KeyType.GREATER_THAN,
        b"?": KeyType.QUESTION_MARK,
        b"@": KeyType.AT_SIGN,
        b"[": KeyType.LEFT_SQUARE_BRACKET,
        b"\\": KeyType.BACKSLASH,
        b"]": KeyType.RIGHT_SQUARE_BRACKET,
        b"^": KeyType.CARET,
        b"_": KeyType.UNDERSCORE,
        b"`": KeyType.GRAVE_ACCENT,
        b"{": KeyType.LEFT_CURLY_BRACE,
        b"|": KeyType.VERTICAL_BAR,
        b"}": KeyType.RIGHT_CURLY_BRACE,
        b"~": KeyType.TILDE,
    }.get(character, None)

    return k_type, None


def detect_control_key_type(character) -> KeyType | None:
    if b"\x01" <= character <= b"\x1a":
        return KeyType[chr(ord(character) + 64)]

    k_type = {
        b"\x00": KeyType.AT_SIGN,
        b"\x1b": KeyType.LEFT_SQUARE_BRACKET,
        b"\x1c": KeyType.BACKSLASH,
        b"\x1d": KeyType.RIGHT_SQUARE_BRACKET,
        b"\x1e": KeyType.CARET,
        b"\x1f": KeyType.UNDERSCORE,
    }.get(character, None)

    return k_type


def detect_key(keys: list[bytes]):
    value = b"".join(keys).decode("utf-8")
    arrow_directions = {
        b"A": KeyType.UP,
        b"B": KeyType.DOWN,
        b"C": KeyType.RIGHT,
        b"D": KeyType.LEFT,
        b"F": KeyType.END,
        b"H": KeyType.HOME,
    }
    mods_dict = {
        b"2": (ModKey.SHIFT,),
        b"3": (ModKey.ALT,),
        b"4": ( ModKey.ALT, ModKey.SHIFT),
        b"5": (ModKey.CONTROL,),
        b"6": ( ModKey.CONTROL, ModKey.SHIFT),
        b"7": ( ModKey.CONTROL, ModKey.ALT),
        b"8": ( ModKey.CONTROL, ModKey.ALT, ModKey.SHIFT),
    }
    match keys:

        case [k] | [b"\x1b", k]:
            mods = []
            if len(keys) == 2:
                mods.append(ModKey.ALT)

            if k_type := detect_control_key_type(k):
                mods.append(ModKey.CONTROL)
                return Key(
                    type=k_type, value=value, mods=tuple(mods), pasted=False
                )

            k_type, mod = detect_printable_key_type(k)
            if k_type is not None:
                if mod is not None:
                    mods.append(mod)
                return Key(
                    type=k_type, value=value, mods=tuple(mods), pasted=False
                )

            if k == b"\x7f":
                return Key(
                    type=KeyType.BS,
                    value=value,
                    mods=tuple(mods),
                    pasted=False,
                )

            return None

        case [
            b"\x1b",
            b"[",
            direction,
        ] if direction in arrow_directions.keys():
            k_type = arrow_directions.get(direction)
            return Key(type=k_type, value=value, mods=tuple(), pasted=False)

        case [b"\x1b", b"[", b"3", b"~"]:
            return Key(
                type=KeyType.DELETE, value=value, mods=tuple(), pasted=False
            )

        case [b"\x1b", b"[", b"3", b";", mods_coef, b"~"]:
            mods = mods_dict.get(mods_coef)
            return Key(
                type=KeyType.DELETE, value=value, mods=mods, pasted=False
            )

        case [
            b"\x1b",
            b"[",
            b"1",
            b";",
            mods_coef,
            direction,
        ] if direction in arrow_directions.keys():
            mods = mods_dict.get(mods_coef)
            k_type = arrow_directions.get(direction)
            return Key(type=k_type, value=value, mods=mods, pasted=False)

        case _:
            return None


def interpret_key_bytes(keys: list[bytes]) -> tuple[Key | None, bool]:
    key, ok = detect_bracketed_input(keys)

    if not ok:
        return key, True

    if ok and key is not None:
        return key, False

    if key := detect_key(keys):
        return key, False

    value = b"".join(keys).decode("utf-8")
    return (
        Key(type=KeyType.Text, value=value, mods=tuple(), pasted=False),
        False,
    )


if __name__ == "__main__":
    print("\x1b[?2004h", end="")
    c = input("> ")
    ba = bytearray(c, "utf-8")
    key = detect_key([b.to_bytes() for b in ba])
    print(key)
