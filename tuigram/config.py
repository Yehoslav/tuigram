"""
Every parameter (except for CONFIG_FILE) can be
overwritten by external config file
"""
import os
import platform
import runpy
from typing import Any, Dict, List, Optional, Tuple
from tuigram.components.input_box import vim_key_handler

_os_name = platform.system()
_linux = "Linux"


CONFIG_DIR: str = os.path.expanduser("~/.config/tuigram/")
CONFIG_FILE: str = os.path.join(CONFIG_DIR, "config.py")
FILES_DIR: str = os.path.expanduser("~/.cache/tuigram/")
DRAFTS_FILE: str = os.path.join(FILES_DIR, "drafts.json")
MAILCAP_FILE: Optional[str] = None
MAILCAP: dict[str, str] = {}
HISTORY_FILE: str = os.path.expanduser("~/.local/state/tuigram/history")

LOG_LEVEL: str = "INFO"
LOG_PATH: str = os.path.expanduser("~/.local/share/tuigram/")

API_ID: int = 14489761
API_HASH: str = "cb4d0d208c5fbc2c631c2c53c8e66018"

PHONE: Optional[str] = None
ENC_KEY: str = ""

TDLIB_PATH: Optional[str] = None
TDLIB_VERBOSITY: int = 0

MAX_DOWNLOAD_SIZE: str = "10MB"

MSG_LEFT_PADDING: int = 8
MSG_MAX_WIDTH: int = 80
MSG_MAX_LINES: int = 10

NOTIFY_FUNCTION: Optional[Any] = None

INPUT_SHOW_MODE: bool = True
INPUT_KEY_HANDLER = vim_key_handler

VIEW_TEXT_CMD: str = "less"
VIEW_PHOTOS_CMD: str = "nsxiv {file_path} -t"

# for more info see https://trac.ffmpeg.org/wiki/Capture/ALSA
VOICE_RECORD_CMD: str = (
    "ffmpeg -f alsa -i hw:0 -c:a libopus -b:a 32k {file_path}"
    if _os_name == _linux
    else "ffmpeg -f avfoundation -i ':0' -c:a libopus -b:a 32k {file_path}"
)

EDITOR: str = os.environ.get("EDITOR", "vim")

LONG_MSG_CMD: str = f"{EDITOR} '{{file_path}}'"

DEFAULT_OPEN: str = (
    "xdg-open {file_path}" if _os_name == _linux else "open {file_path}"
)

CHAT_FLAGS: Dict[str, str] = {}

MSG_FLAGS: Dict[str, str] = {}

ICON_PATH: str = os.path.join(
    os.path.dirname(__file__), "resources", "tuigram.png"
)

URL_VIEW: Optional[str] = None

USERS_COLOURS: Tuple[int, ...] = tuple(range(2, 16))

KEEP_MEDIA: int = 7

FILE_PICKER_CMD: Optional[str] = None

DOWNLOAD_DIR: str = os.path.expanduser("~/Downloads/")

EXTRA_FILE_CHOOSER_PATHS: List[str] = ["..", "/", "~"]

CUSTOM_KEYBINDS: Dict[str, Dict[str, Any]] = {}

TRUNCATE_LIMIT: int = 15

DECODE_INPUT_ESCAPES: bool = True

EXTRA_TDLIB_HEADEARS: Dict[Any, Any] = {}

if os.path.isfile(CONFIG_FILE):
    config_params = runpy.run_path(CONFIG_FILE)
    for param, value in config_params.items():
        if param.isupper():
            globals()[param] = value
else:
    os.makedirs(CONFIG_DIR, exist_ok=True)

    if not PHONE:
        print(
            "Enter your phone number in international format, including country code (example: +5037754762346)"
        )
        PHONE = input("(phone) ")
        if not PHONE.startswith("+"):
            PHONE = "+" + PHONE

    with open(CONFIG_FILE, "a") as f:
        f.write(f'\nPHONE = "{PHONE}"\n')
