from setuptools import setup

import tuigram

with open("README.md", "r") as fh:
    readme = fh.read()

with open("requirements.txt", "r") as fr:
    reqs = list(map(str.strip, fr.readlines()))

setup(
    long_description=readme,
    long_description_content_type="text/markdown",
    name="tuigram",
    version=tuigram.__version__,
    description="A fork of arigram -- a hackable telegram TUI client",
    url="",
    author="Yehoward",
    author_email="rudencoyehoward@gmail.com",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
    ],
    packages=["tuigram"],
    entry_points={"console_scripts": ["tuigram = tuigram.__main__:main"]},
    python_requires=">=3.10",
    install_requires=reqs,
)
